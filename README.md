# Open Source

## An open source novel about the power of human collaboration

***Open Source*** is a young-adult novel about how humans can self-oragnize and overcome any obstacle when they are willing to be transparent, helpful, and collaborative.
It's a book that embodies its theme not only within the story itself, but also in the method that it is being written.

The novel is being written publically here on GitHub in a fully collaborative environment.
Every version of every draft is available for review, critique and editing.
If you would like to contribute to the writing of this novel, please feel free to do so!

## How to contribute

Start by checking out the **Issues** page to see if there are any outstanding suggestions that have not been implemented yet.
Once you have an idea of what you want to do to contribute - which could be anything from fixing typos, to writing scenes, to fixing dialogue, to restructuring the order of the book - clone the project onto your local machine, make changes, and submit a pull request.
We'll review the pull request and have a conversation about the implications of those changes.
Once everyone's happy about the changes you've made, we'll merge it into the book for posterity!

Alternatively, we could use as many readers as possible.
A book lives and breathes by how well it's critiqued, and that's one of the things that open source software really shines in.
Because the book is public throughout the entire writing process, please feel free to read it in its raw alpha state.
Create issues for all the things that you think suck.

## HALP!? When you GitHub, what do you do?????!

I'm aware that using GitHub - or even just Git - to write a novel is not part of most author's writing flow.
You probably don't have the foggiest about how to use all these complicated programmer's tools.
That's fine.
I didn't know how to use them until I started programming.

However, you'll need to lern some basics if you want to contribute through actual writing.

I'll update this section as I compile better resources in the future, but in the mean time, you'll need a Git client and a Text Editor.
I recommend using *SourceTree* as a visual representation of Git, and something like *Atom* as the tool you use to write the documents themselves.

Feel free to watch YouTube videos about getting either set up.
Programmers like documenting things, so there are myriad resources that will show you how to get your "development environment" up and running.
