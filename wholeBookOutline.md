# Save the Cat!: A Cheat Sheet

## A Worthy Hero

1. **A problem** (or flaw that needs fixing) 
2. **A want** (or goal that the hero is pursuing) 
3. **A need** (or life lesson to be learned)

## Genre (keep only one)

1. **WHYDUNIT**: A mystery must be solved by a hero (who may or may not be a detective) during which something shocking is revealed about the dark side of human nature. (See chapter 4.) 
    1. Detective
    2. Secret
    3. Dark turn
2. **RITES OF PASSAGE**: A hero must endure the pain and torment brought about by life’s common challenges (death, separation, loss, divorce, addiction, coming of age, and so on). (See chapter 5.) 
    1. Life problem
    2. Wrong way to attack the problem
    3. Solution to the problem
3. **INSTITUTIONALIZED**: A hero enters or is already entrenched inside a certain group, institution, establishment, or family and must make a choice to join, escape, or destroy it. (See chapter 6.)
    1. Group
    2. Choice
    3. Sacrifice
4. **SUPERHERO**: An extraordinary hero finds themselves in an ordinary world and must come to terms with being special or destined for greatness. (See chapter 7.)
    1. Hero with special power
    2. Nemesis
    3. Curse the hero suffers
5. **DUDE WITH A PROBLEM**: An innocent, ordinary hero suddenly finds themselves in the midst of extraordinary circumstances and must rise to the challenge. (See chapter 8.)
    1. Innocent hero
    2. Sudden event
    3. Life-or-death battle
6. **FOOL TRIUMPHANT**: An underestimated, underdog hero is pitted against some kind of “establishment” and proves a hidden worth to society. (See chapter 9.)
    1. A fool
    2. Establishment the fool is pitted against
    3. Transmutation of fool into someone else/new
7. **BUDDY LOVE**: A hero is transformed by meeting someone else, including (but not limited to) love stories, friendship stories, and pet stories. (See chapter 10.)
    1. An incomplete hero
    2. Hero’s counterpart
    3. Complication
8. **OUT OF THE BOTTLE**: An ordinary hero is temporarily “touched by magic,” usually involving a wish fulfilled or a curse bestowed, and the hero learns an important lesson about appreciating and making the most of “reality.” (See chapter 11.)
    1. Hero deserving of some magic
    2. The magic
    3. A lesson learned
9. **GOLDEN FLEECE**: A hero (or group) goes on a “road trip” of some type (even if there’s no actual road), in search of one thing and winds up discovering something else—themselves. (See chapter 12.)
    1. Road
    2. Team
    3. Prize
10. **MONSTER IN THE HOUSE**: A hero (or group of heroes) must overcome some kind of monster (supernatural or not), in some kind of enclosed setting (or limited circumstances), and someone is usually responsible for bringing the monster into being. (See chapter 13.)
    1. Monster
    2. House
    3. Sin

## Beat Sheet

1. **Opening Image** – A before “snapshot” of your main character/hero and their world 
2. **Theme Stated** – A statement made by a character (normally not the hero) that hints at what the hero’s arc will be (i.e., what the hero must learn/discover before the end of the book.) 
3. **Set-up** – An exploration of the hero’s status quo life and all its flaws. This is where we learn what the hero’s life and world look like before its epic transformation. Includes the **stasis = death** moment.
4. **<span style="text-decoration:underline;">(1) Catalyst </span>**– An inciting incident that happens to the hero which will catapult them into a new world or new way of thinking. 
5. **Debate** – A reaction sequence in which the hero debates what they will do next, usually presented in the form of a question. 
6. **<span style="text-decoration:underline;">(2) Break into 2</span>** – The moment the hero decides to accept the call to action, leave their comfort zone, try something new, venture into a new world or new way of thinking. 
7. **B Story** – The introduction of a new character or characters who will ultimately serve to help the hero learn the theme. 
8. **Fun and Games** – This is where we see the hero in their new world. Also called “the Promise of the Premise,” this section represents the “hook” of the story. Why the reader picked up the novel in the first place. 
9. **<span style="text-decoration:underline;">(3) Midpoint </span>**– Literally the middle of the novel, where the “Fun and Games” culminates in either a “false victory” or a “false defeat.” Something should happen here to “raise the stakes” and push the hero toward real change. 
10. **Bad Guys Close In** – If the midpoint was a false victory, this section will be a downward trajectory where things get consistently worse for the hero. If the midpoint was a false defeat, this section will be an upward trajectory where things get seemingly better. Regardless of trajectory, the hero’s inner demons or “internal bad guys” are also closing in. 
11. **<span style="text-decoration:underline;">(5) All is Lost </span>**– The lowest point of the novel. This is an action beat where something happens to the hero that, combined with those “internal bad guys,” pushes them to rock bottom. 
12. **Dark Night of the Soul** – Another reaction beat (similar to the Debate) where the hero takes a moment to react to everything that’s happened leading up to this moment. The darkest night before the dawn, this is the moment right before the hero figures out the solution to their big problem and learns the theme. 
13. **<span style="text-decoration:underline;">(4) Break into 3</span>** – The aha moment! The hero realizes what they must do to not only fix all of the problems created in act 2, but more importantly, fix themselves. 
14. **Finale** – The hero proves they have truly learned the theme and enacts the plan they came up with in the “Break into 3.” Bad guys are destroyed, inner demons are conquered, lovers are reunited. The hero’s world is not only saved…it’s a better place than it was before. 
    1. **Gathering the Team** – The protagonist rounds up his or her friends, and gathers the tools, weapons, and supplies needed to execute the plan.
    2. **Executing the Plan** – The protagonist (and his or her crew) execute the plan. Sometimes secondary characters are sacrificed here in order to force the protagonist to continue forward on their own.
    3. **The High Tower Surprise** – The protagonist faces a twist or a surprise that forces him or her to prove their worth.
    4. **Dig Deep Down** – With no backup plan, the protagonist has to dig deep inside themselves to find the most important weapon of them all—the strength and courage to overcome their fear or false belief (internal antagonist) and face the antagonist or antagonistic force (external antagonist).
    5. **Execution of New Plan** – After the protagonist overcomes their fear or false belief (internal antagonist), he or she takes action against the antagonist or antagonistic force (external antagonist) and is successful. (If you’re writing a story where the protagonist isn’t successful, make sure there’s a _point_ to their failure.)
15. **Final Image** – A mirror to the “Opening Image”, this is the closing “snapshot” of who the hero is now that he’s gone through this epic and satisfying transformation.

## LOGLINE TEMPLATE

“On the verge of a **stasis = death** moment, a flawed hero **Breaks Into 2**; but when the **Midpoint** happens, they must learn the **Theme Stated** before the **All Is Lost**.”

## SYNOPSIS TEMPLATE

**PARAGRAPH 1**: Setup, flawed hero, and Catalyst (2–4 sentences) 

**PARAGRAPH 2**: Break Into 2 and/or Fun and Games (2–4 sentences) 

**PARAGRAPH 3**: Theme Stated, Midpoint hint and/or All Is Lost hint, ending in a cliffhanger (1 to 3 sentences)
